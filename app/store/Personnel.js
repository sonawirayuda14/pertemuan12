Ext.define('Pertemuan2.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',
    alias: 'store.personnel',
    
    /*autoLoad: true,*/

    fields: [
        'gambar', 'npm', 'name', 'email', 'phone', 'address', 'angkatan'
    ],

    data: { items: [
        { gambar: 'sona.jpg', angkatan: '18', npm: '183510881', name: 'Sona Firdaus Wirayuda', email: "sonafirdauswirayuda@student.uir.ac.id", phone: "555-111-1111", address: "Jl. Teropong Perum GSP II" },
        { gambar: 'sonafw.jpg', angkatan: '17', npm: '173510542', name: 'Wirayuda Firdaus Sona',     email: "wirayudafirdaussona@student.uir.ac.id",  phone: "555-222-2222", address: "Jl. Karya I" },
        { gambar: 'sona.jpg', angkatan: '18', npm: '183510324', name: 'Firdaus Sona Wirayuda',   email: "firdaussonawirayuda@student.uir.ac.id",    phone: "555-333-3333", address: "Jl. Air Dingin" },
        { gambar: 'sonafw.jpg', angkatan: '17', npm: '173510123', name: 'Sona Wirayuda Firdaus',     email: "sonawirayudafirdaus@student.uir.ac.id",        phone: "555-444-4444", address: "Jl. Bareng ga jadian" }
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});

    /*proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/MyApp_php/readPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});*/
