Ext.define('Pertemuan2.view.tree.TreeListModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tree-list',

    formulas: {
        selectionText: function(get) {
            var selection = get('treelist.selection'),
                path;
            if (selection) {
                path = selection.getPath('text');
                path = path.replace(/^\/Root/, '');
                return 'Selected: ' + path;
            } else {
                return 'No node selected';
            }
        }
    }, 

    stores: {
        navItems: {
            type: 'tree',
            rootVisible: true,
            root: {
                expanded: true,
                text: 'Angkatan Mahasiswa Fakultas Teknik',
                iconCls: 'x-fa fa-university',
                children: [{
                    text: 'Mahasiswa Teknik Sipil',
                    iconCls: 'x-fa fa-users',
                        children: [{
                            text: 'Belum ada Mahasiswa yang terdaftar',
                            iconCls: 'x-fa fa-minus',
                            leaf: true
                        }]
                    },{
                    text: 'Mahasiswa Teknik Perminyakan',
                    iconCls: 'x-fa fa-users',
                        children: [{
                            text: 'Belum ada Mahasiswa yang terdaftar',
                            iconCls: 'x-fa fa-minus',
                            leaf: true
                        }]
                    },{
                    text: 'Mahasiswa Teknik Mesin',
                    iconCls: 'x-fa fa-users',
                        children: [{
                            text: 'Belum ada Mahasiswa yang terdaftar',
                            iconCls: 'x-fa fa-minus',
                            leaf: true
                        }]
                    },{
                    text: 'Mahasiswa Teknik Perencanaan Wilayah Kota',
                    iconCls: 'x-fa fa-users',
                        children: [{
                            text: 'Belum ada Mahasiswa yang terdaftar',
                            iconCls: 'x-fa fa-minus',
                            leaf: true
                        }]
                    },{
                    text: 'Mahasiswa Teknik Informatika',
                    iconCls: 'x-fa fa-users',
                        children: [{
                            text: '18',
                            iconCls: 'x-fa fa-play',
                            leaf: true
                        }, {
                            text: '17',
                            iconCls: 'x-fa fa-play',
                            leaf: true
                        }]
                    },{
                    text: 'Mahasiswa Teknik Geologi',
                    iconCls: 'x-fa fa-users',
                        children: [{
                            text: 'Belum ada Mahasiswa yang terdaftar',
                            iconCls: 'x-fa fa-minus',
                            leaf: true
                        }]
                    }]
                }
            }
        }
});