Ext.define('Pertemuan2.view.tree.TreeList', {
    extend: 'Ext.grid.Tree',
    xtype: 'tree-list',
    requires: [
        'Ext.grid.plugin.MultiSelection'
    ],

    
    cls: 'demo-solid-background',
    shadow: true,

    viewModel: {
        type: 'tree-list'
    },

    bind: '{navItems}',

    listeners: {
        itemtap: function ( me, index, target, record, e, eOpts ){
            detailtree = Ext.getCmp('detailtree');
            detailtree = Ext.getStore('personnel');
            detailtree.filter('angkatan',record.data.text);
        } 
    }
});