Ext.define('Pertemuan2.view.tree.TreePanel', {
    extend: 'Ext.Container',
    xtype: 'tree-panel',

    requires: [
        'Ext.layout.HBox',
        'Pertemuan2.view.tree.TreeList',
        'Pertemuan2.view.setting.Setting'
    ],

    layout: {
        type: 'hbox',
        pack: 'center',
        align: 'stretch'
    },
    margin: '0 10',
    defaults: {
        margin: '0 0 10 0',
        bodyPadding: 10
    },
    items: [
        {
            xtype: 'tree-list',
            flex: 1
        },
        {
            xtype: 'panel',
            title : 'Data Mahasiswa Angkatan',
            id: 'detailtree',
            flex: 1,
            items:[{
                xtype: 'basicdataview'
            }]
        }
    ]
})