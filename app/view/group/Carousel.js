Ext.define('Pertemuan2.view.group.Carousel', {
    extend: 'Ext.Container',
    xtype: 'mycarousel',
    requires: [
        'Ext.carousel.Carousel'
    ],

    shadow: true,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    /* defaults: {
        flex: 1
    },*/ 
    items: [{
        xtype: 'carousel',
        width: 300,
        id: 'carouselatas',
        items: [{
            html: '<p>Ext JS provides the industrys most comprehensive collection of high-performance, customizable UI widgets - including HTML5 grids, trees, lists, forms, menus, toolbars, panels, windows, and much more. Ext JS leverages HTML5 features on modern browsers while maintaining compatibility and functionality for legacy browsers. Use these examples to build your cross-platform apps for desktop, tablets, and smartphones.</p>'
        },
        {
            html: '<p>You can also tap on either side of the indicators.</p>'
        },
        {
            html: 'Card #3'
        }]
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'vbox'
            },
            items: [{

            },{
                    text: 'Back to Users',
                    ui: 'action',
                    handler: function(){
                        alert('Kita Akan Kembali ke Users');
                        var mainView = Ext.getCmp('app-main');
                        mainView.setActiveItem(1);
                    }
            },{
                
            }]
    }, {
        xtype: 'spacer'
    }, {
        xtype: 'carousel',
        width: 300,
        direction: 'vertical',
        items: [{
            html: '<p>Ext JS provides the industrys most comprehensive collection of high-performance, customizable UI widgets - including HTML5 grids, trees, lists, forms, menus, toolbars, panels, windows, and much more. Ext JS leverages HTML5 features on modern browsers while maintaining compatibility and functionality for legacy browsers. Use these examples to build your cross-platform apps for desktop, tablets, and smartphones.</p>'
        },
        {
            html: 'And can also use <code>ui:light</code>.'
        },
        {
            html: 'Card #3'
        }]
    }]
});