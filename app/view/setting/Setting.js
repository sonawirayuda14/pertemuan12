Ext.define('Pertemuan2.view.setting.Setting', {
    extend: 'Ext.Container',
    xtype: 'basicdataview',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Pertemuan2.store.Personnel',
        'Ext.field.Search'
    ],

    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },

    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [{
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'searchfield',
                    placeHolder: 'Search NPM',
                    name: 'searchfield',
                    listeners: {
                        change: function ( me, newValue, oldValue, eOpts ){
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('npm', newValue);
                        }
                    }
                },{
                    xtype: 'spacer'
                },{
                    xtype: 'searchfield',
                    placeHolder: 'Search Name',
                    name: 'searchfield',
                    listeners: {
                        change: function ( me, newValue, oldValue, eOpts ){
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('name', newValue);
                        }
                    }
                },{
                    xtype: 'spacer'
                },{
                    xtype: 'searchfield',
                    placeHolder: 'Search Email',
                    name: 'searchfield',
                    listeners: {
                        change: function ( me, newValue, oldValue, eOpts ){
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('email', newValue);
                        }
                    }
                }
            ]
        },{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl: '<img src=/resources/{gambar} width=150 height=150 class="gambar"/><br><b><font size=4>{name}</font><br><font size=2>{npm}</font><hr>',
        bind: {
            store: '{personnel}',
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            delegate: '.gambar',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>--BIODATA--</td>' +
                    '<tr><td>NPM: </td><td>{npm}</td></tr>' +
                    '<tr><td>Name:</td><td>{name}</td></tr>' +
                    '<tr><td>Angkatan:</td><td>{angkatan}</td></tr>' +
                    '<tr><td>Email:</td><td>{email}</td></tr>' +
                    '<tr><td>Phone:</td><td>{phone}</td></tr>' +
                    '<tr><td>Address:</td><td>{address}</td></tr>' 
        }
    }]
});