/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Pertemuan2.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',
    requires: [
        'Ext.MessageBox',

        'Pertemuan2.view.main.MainController',
        'Pertemuan2.view.main.MainModel',
        'Pertemuan2.view.main.List',
        'Pertemuan2.view.form.User',
        'Pertemuan2.view.group.Carousel',
        'Pertemuan2.view.setting.Setting',
        'Pertemuan2.view.form.Login',
        'Pertemuan2.view.chart.Column',
        'Pertemuan2.view.chart.Bar',
        'Pertemuan2.view.chart.PieBasic',
        'Pertemuan2.view.tree.TreePanel'     
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'mainlist'
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'user'
            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'mycarousel'
            }]
        },{
            title: 'Dataview',
            iconCls: 'x-fa fa-tag',
            layout: 'fit',
            items: [{
                xtype: 'basicdataview'
            }]
        },{
            title: 'Chart',
            iconCls: 'x-fa fa-star',
            layout: 'fit',
            items: [{
                xtype: 'column-chart'
            }]
        },{
            title: 'Bar',
            iconCls: 'x-fa fa-star',
            layout: 'fit',
            items: [{
                xtype: 'bar' 
            }]
        },{
            title: 'Pie',
            iconCls: 'x-fa fa-star',
            layout: 'fit',
            items: [{
                xtype: 'piebasic' 
            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-wrench',
            layout: 'fit',
            items: [{
                xtype: 'tree-panel'
            }]
        }
    ]
});
