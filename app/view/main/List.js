/**
 * This view is an example list of people.
 */
Ext.define('Pertemuan2.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Pertemuan2.store.Personnel'
    ],

    title: 'Data Anggota Sencha - Sona Firdaus Wirayuda',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'NPM',  dataIndex: 'npm', width: 120 },
        { text: 'Nama',  dataIndex: 'name', width: 200 },
        { text: 'Angkatan',  dataIndex: 'angkatan', width: 200 },
        { text: 'Email', dataIndex: 'email', width: 230 },
        { text: 'Telepon', dataIndex: 'phone', width: 150 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
